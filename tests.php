/**
 * Test 1: this should produce 'Today's lunch will be: peanut and honey butter sandwich' as the suggested lunch
 *
 * @param $input array       (the ingredients array)
 * @param $ingredient string the ingredient name index which the ingredientsArray will be attached to)
 * @param $ingredientsArray  (the array of ingredients to add)
 * @return array|bool
 */
function addIngredient ( $input, $ingredient, $ingredientsArray) {

    if ($input && $ingredientsArray) {

        // add the new ingredient
        $input[count($input)] = array($ingredient => $ingredientsArray);

        // return the input with new ingredient
        return $input;
    }
    return false;
}
//$array = array(200,'grams','22/07/2017');
//if (addIngredient( $ingredients, 'honey', $array)) {
//    $ingredients = addIngredient( $ingredients, 'honey', $array);
//}

/**
 * Test 2: this should produce 'You need a new Fridge!' as the message
 */