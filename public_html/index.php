<?php
/**
 * Created by PhpStorm.
 * User: gilbertr
 * Date: 20/07/2017
 * Time: 12:50 AM
 *
 * This application requires two input files:
 *      1) CSV containing ingredients
 *      2) JSON containing recipes
 */

// set a timezone to satisfy strtotime usage
date_default_timezone_set('Australia/Sydney');

// load the csv file
$fridgeCsv   = fopen("../assets/fridge.csv", "r");
$index       = 0; // use this to get the headers
$headers     = array();
$ingredients = array();

// process the CSV into an array
if ($fridgeCsv !== false) {

    // if ingredients exists iterate the list
    while (($line = fgetcsv($fridgeCsv, 1000, ",")) !== false) {

        if ($index === 0) {

            // capture the headers
            foreach ($line as $header) {
                $headers[] = $header;
            }
            $index++;
            continue;
        }

        $i = 0; // use this (as zero) the get the ingredients name
        $array = array();
        // preserve the ingredient name
        $name  = null;
        foreach ($line as $column) {

            // make the ingredient name the key
            if ($i === 0) {

                $name = $column;
                $array[$name] = array();
                $i++;
                continue;
            }
            // create an array of the ingredients parameters
            $array[$name][$headers[$i]] = $column;
            $i++;
        }
        // add to the main array
        $ingredients[] = $array;
    }
}

// clear some memory (in case we're running on an old 586)
unset($fridgeCsv);
unset($line);
unset($headers);
unset($header);
// we now have an array ($ingredients) of ingredients with numeric keys and internal array with the ingredient name as the key

// load the JSON file and process the object(s) into an array
$recipesJson = file_get_contents("../assets/recipes.json");
$objects     = json_decode($recipesJson);
$array       = array();
$recipes     = array(); // this array wil contains the recipes after processing
$name        = null;

if (isset($objects) && count($objects)) {
    foreach ($objects as $recipe) {

        foreach ($recipe as $key => $row) {

            // process the ingredients into an array
            if ($key == 'ingredients') {

                $arr = array();
                // the ingredients 'object' may have multiple rows of requirements
                foreach ($row as $i => $o) {

                    $a = array();
                    foreach ($o as $k => $v) {
                        // rebuild the individual array
                        $a[$k] = $v;
                    }
                    // reassemble each with the numeric key
                    $arr[$i] = $a;
                }
                // stick the 'ingredients array back together'
                $array[$key] = $arr;
                continue;
            }
            // save the recipe name
            $name = $row;
        }
        // add the recipe to the array with its requirement as an internal array
        $recipes[$name] = $array;
    }
}

// clear some memory (in case we're running on an old 586)
unset($recipesJson);
unset($objects);
unset($recipe);
unset($row);
unset($arr);
unset($array);
// we now have an array ($recipes) of recipes with the recipe name as the key

/**
 * Test 1: this should produce 'Today's lunch will be: peanut and honey butter sandwich' as the suggested lunch
 *
 * Add ?test=1 (2,3,4) to initialise the tests
 *
 * @param $input array       (the ingredients array)
 * @param $ingredient string the ingredient name index which the ingredientsArray will be attached to)
 * @param $ingredientsArray  (the array of ingredients to add)
 * @return array|bool
 */
function addIngredient ( $input, $ingredient, $ingredientsArray) {

    if ($input && $ingredientsArray) {

        // add the new ingredient
        $input[count($input)] = array($ingredient => $ingredientsArray);

        // return the input with new ingredient
        return $input;
    }
    return false;
}
if (isset($_GET['test']) && $_GET['test'] == 1) {
    $newIngredient = array(200,'grams',date('d/m/Y', time()+60*60*24));
    if (addIngredient( $ingredients, 'honey', $newIngredient)) {
        $ingredients = addIngredient( $ingredients, 'honey', $newIngredient);
    }
}

/**
 * Test 2: this should produce 'You need a new Fridge!' as the message
 *
 */
if (isset($_GET['test']) && $_GET['test'] == 2) { 
    $ingredients = array();
}

/**
 * Test 3: this should produce 'Better learn how to cook!' as the message
 *
 */
if (isset($_GET['test']) && $_GET['test'] == 2) { 
    $recipes = array();
}    

/**
 * Test 4: this should produce 'Martians have invaded ~' warning as the message
 *
 */
if (isset($_GET['test']) && $_GET['test'] == 2) {  
    $recipes = $ingredients = array();
}    

// only continue if both arrays are present and containing records
if (isset($ingredients, $recipes) && count($ingredients) >= 1 && count($recipes) >= 1) {

    // capture the output into an array
    // create an array with an internal array using a timestamp key representing an analysis of the use-by dates
    $output = array();

    // iterate through the recipes
    foreach ($recipes as $name => $requirements) {

        // iterate through the recipe requirements
        foreach ($requirements as $requirement) {

            // get the number of requirements and track the number found
            $numberRequirements = count($requirement);
            $foundRequirements  = 0;
            $now                = time();
            $timestamps         = array();

            foreach ($requirement as $row) {

                // iterate through the ingredients and find matching required items
                foreach ($ingredients as $array) {

                    foreach ($array as $item => $arr) {

                        if (current($row) == $item) {

                            // the items match - now check the amounts and use-by date
                            if (current($arr) >= next($row) && next($arr) == next($row)) {

                                // if the use-by timestamp is greater than or equal to now consider the available ingredient as usable
                                $ubd = strtotime(str_replace("/","-", next($arr)));
                                if ($ubd >= $now) {
                                    $timestamps[] = $ubd;
                                    $foundRequirements++;
                                }
                            }
                        }
                    }
                }
            }

            // compare the ~Requirements vars
            if ($foundRequirements == $numberRequirements) {

                // the current recipe can be used
                // reverse sort the saved use-by timestamps -  use the oldest item as the output key for the current recipe
                sort($timestamps); // this will sort lowest to highest
                $output[] = array($timestamps[0] => $name);
            }
        }
    }

    if ($output && count($output) >= 1) {

        // one or more recipes have past the tests - manually sort the output
        $timestamp = 0;
        $lunch     = null;
        foreach ($output as $key => $array) {
            foreach ($array as $time => $name) {
                if (strlen($time) && ($timestamp == 0 || $time < $timestamp)) {
                    $timestamp = $time;
                    $lunch = $name;
                }
            }
        }        

        echo "<pre>Today's lunch will be: " . $lunch . "...</pre>";

    } else {

        // either no recipes were matched or all items are out of date
        echo "<pre>Today's lunch will be: Burger King!</pre>";
    }

} elseif (isset($recipes) && count($recipes) && (!isset($ingredients) || count($ingredients) == 0)) {

    // no ingredients available
    echo "<pre>You need a new Fridge!</pre>";

} elseif (isset($ingredients) && count($ingredients) && (!isset($recipes) || count($recipes) == 0)) {

    // no recipes available
    echo "<pre>Better learn how to cook!</pre>";

} else {

    // no hope !!
    echo "<pre>Martians have invaded! Flee for your lives!</pre>";

}

